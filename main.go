package main

import (
	"net/http"

	"edspert.id/course/cicd/internal/handler"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "hello",
		})
	})
	r.GET("/world", handler.HelloWorld)

	r.POST("/sum", handler.SumHandler)
	r.POST("/multiply", handler.MultiplicationHandler)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
