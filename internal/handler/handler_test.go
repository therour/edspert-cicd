package handler

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/goccy/go-json"
)

func TestSumHandler(t *testing.T) {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	body := numbersBody{
		X: 9,
		Y: 3,
	}
	jsonBody, _ := json.Marshal(body)

	fmt.Println(string(jsonBody))

	ctx.Request = httptest.NewRequest("POST", "/", bytes.NewBuffer(jsonBody))

	SumHandler(ctx)

	if w.Code != http.StatusOK {
		t.Fatal("status code is not 200 but got", w.Code)
	}

	bodyResponse := make(map[string]interface{})

	err := json.Unmarshal(w.Body.Bytes(), &bodyResponse)
	if err != nil {
		t.Fatal("Cannot parse response body", err)
	}

	if bodyResponse["Result"].(float64) != 12 {
		t.Fatal("Result is not 12, but got", bodyResponse["Result"])
	}
}

func TestMultiplicationHandler(t *testing.T) {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	body := numbersBody{
		X: 9,
		Y: 3,
	}
	jsonBody, _ := json.Marshal(body)

	fmt.Println(string(jsonBody))

	ctx.Request = httptest.NewRequest("POST", "/", bytes.NewBuffer(jsonBody))

	MultiplicationHandler(ctx)

	if w.Code != http.StatusOK {
		t.Fatal("status code is not 200 but got", w.Code)
	}

	bodyResponse := make(map[string]interface{})

	err := json.Unmarshal(w.Body.Bytes(), &bodyResponse)
	if err != nil {
		t.Fatal("Cannot parse response body", err)
	}

	if bodyResponse["Result"].(float64) != 27 {
		t.Fatal("Result is not 12, but got", bodyResponse["Result"])
	}
}
